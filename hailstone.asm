; Hailstone Sequence - Written by Chase Opsahl
; 12/07/2018
;
; In the future I would like to use local variables as counters instead of registers.
; Using registers proved to be problematic becuase everytime I would call a function
; it would mess up the registers. This was really annoying xD.

segment .data

        ask             db              "Enter a number greater than 1 to get it's hailstone sequence: ",0
        hail            db              "Hailstone Sequence for %d:",10,0
        stone           db              "It took %d calculation(s) to get to 1!",10,0
        invalid         db              "Invalid number. Enter a number greater than 1!",10,0

        pfmt            db              "%d ",0
        sfmt            db              "%d",0
        pnl             db              "",10,0
segment .bss

        usernum         resd    1
        seq             resd    200

segment .text
        global  main
        extern  printf
        extern  scanf

main:
        push    ebp
        mov     ebp, esp

        sub     esp, 4

        ; Get the users number
        push    ask
        call    printf
        add     esp, 4

        push    usernum
        push    sfmt
        call    scanf
        add     esp, 8

        ; validate users number
        cmp     DWORD [usernum], 1
        jge     valid
                ; If number is less than 1
                push    invalid
                call    printf
                add     esp, 4
                jmp     end

        valid:
        ; Load the valid number into the array
        mov     esi, DWORD [usernum]
        mov     DWORD [seq + 0 * 4], esi

        ; Start sequence loop
        mov     ecx, 0                  ; Counter
        mov     ebx, 1                  ; Used to place next number
        start_loop:
        ; Check if number is 1
        cmp     DWORD [seq + ecx * 4], 1
        je      end_loop
                ; If number isn't 1, check if it's even or odd
                mov     eax, DWORD [seq + ecx * 4]
                mov     esi, 2
                cdq
                div     esi
                cmp     edx, 1
                je      odd
                        ; If it is even, move (n/2) into the next position in the array and jmp to start of loop
                        mov     DWORD [seq + ebx * 4], eax
                        jmp     continue

                ; If it is odd, Multiply current number by 3 and add 1. Then stick it in the next position in the array
                odd:
                mov     eax, DWORD [seq + ecx * 4]
                mov     esi, 3
                cdq
                mul     esi
                inc     eax
                mov     DWORD [seq + ebx * 4], eax

        continue:

        inc     ecx
        inc     ebx
        jmp     start_loop
        end_loop:

        mov     DWORD [ebp - 4], ecx

        push    DWORD [usernum]
        push    hail
        call    printf
        add     esp, 8

        mov     edi, DWORD [ebp - 4]    ; copy counter
        mov     esi, 0                                  ; Set new counter.
        print_start:
        cmp     esi, edi
        jg      print_end
                push    DWORD [seq + esi * 4]
                push    pfmt
                call    printf
                add     esp, 8
        inc     esi
        jmp     print_start
        print_end:

        push    pnl
        call    printf
        add     esp, 4

        push    esi
        push    stone
        call    printf
        add     esp, 8

        end:

        mov     esp, ebp
        pop     ebp
        ret
