# Assembly-Problems

These are my solutions to some of the problems on [Rosetta Code](https://rosettacode.org/wiki/Rosetta_Code), written in x86 assembly.
I will be writing mostly in 32bit however once I get comfortable with 32bit I might start using 64bit.

## Problems
* [Hailstone Sequence](https://rosettacode.org/wiki/Hailstone_sequence) based on [Collatz Conjecture](https://xkcd.com/710/)